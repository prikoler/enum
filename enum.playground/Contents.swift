//Создайте по 2 enum с разным типом RawValue.
enum test: Int {
    case Number
    case Square
}

enum Colors {
    case Red(amount: Int)
    case Blue(amount: Int)
    case Green(amount: Int)
}

//Создайте несколько enum для заполнения полей стркутуры - анкета сотрудника: enum пол, enum категория возраста, enum стаж.
enum Gender {
    case male
    case female
}

enum Age {
    case young
    case adult
    case old
}

enum Experience {
    case junior
    case middle
    case senior
}

struct Profile {
    var name: String
    var gender: Gender
    var age: Age
    var experience: Experience
}

let John = Profile(name: "John", gender: .male, age: .young, experience: .middle)

 //Создать enum со всеми цветами радуги. Создать функцию, которая содержит массив разных case'ов enum и выводит содержимое в консоль.
enum Rainbow {
    case Red
    case Orange
    case Yellow
    case Green
    case Blue
    case darkBlue
    case Violet
}

let objects = ["apple", "sun", "house", "car", "moon", "chair", "sofa"]
func phrase (words: Rainbow) {
    switch words {
    case .Red:
        print("Red \(objects[0])")
    case .Orange:
        print("Orange \(objects[1])")
    case .Yellow:
        print("Yellow \(objects[2])")
    case .Green:
        print("Green \(objects[3])")
    case .Blue:
        print("Blue \(objects[4])")
    case .darkBlue:
        print("darkBlue \(objects[5])")
    case .Violet:
        print("Violet \(objects[6])")
    }
}

phrase(words: .Orange)
phrase(words: .Green)

//Создать функцию, которая выставляет оценки ученикам в школе, на входе принимает значение enum Score: String {<Допишите case'ы} и выводит числовое значение оценки
enum Score: String {
    case A = "Отлично"
    case B = "Хорошо"
    case C = "Удовлетворительно"
    case D = "Неудовлетворительно"
}

func getScoreValue(_ score: Score) -> Int {
    switch score {
    case .A:
        return 5
    case .B:
        return 4
    case .C:
        return 3
    case .D:
        return 2
    }
}

let studentScore = Score.A
let scoreValue = getScoreValue(studentScore)

print("Оценка студента - \(scoreValue)")

//Создать метод, которая выводит в консоль какие автомобили стоят в гараже, используйте enum
enum Cars : String {
    case Toyota = "Toyota Land Cruiser"
    case Volkswagen = "Volkswagen Polo"
    case Lamborghini = "Lamborghini Diablo"
    static let all = [Toyota, Volkswagen, Lamborghini]
}

for car in Cars.all {
    print("You have \(car.rawValue) in garage")
}
